//Lesson 2.5
object exercise2_5{


  class Rational (x: Int, y: Int) {

    // default constructor are statements in class body like this:
    require(y != 0, "denominator can not be zero - not a rational")

    // a second constructore with only one parameter
    def this(x: Int) = this(x, 1)

    private def gcd(a: Int, b: Int) : Int =
      if (b == 0) a else gcd(b, a % b)

    val numer = x / gcd(x,y)
    val denom = y / gcd(x,y)

    def add(that: Rational) =
      new Rational (
        this.numer * that.denom + that.numer * this.denom,
        denom * that.denom
      )

    def neg() =
      new Rational (x * -1, y)

    def sub(that: Rational) =
      add(that.neg())

    override def toString = numer + "/" + denom

  }

  val x = new Rational(1,3)
  val y = new Rational(5,7)
  val z = new Rational(3,2)

  x.add(y)

  x.sub(y).sub(z)


}