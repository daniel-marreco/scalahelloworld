import Math.abs

//Lecture 2.3
object exercise4 {

  val tolerance = 0.0001

  def isCloseEnough(x: Double, y: Double) =
    abs((x-y) / x) / x <= tolerance

  // fixed point of f() is the value of x where f(x) = x
  def fixedPoint(f: Double => Double)(firstGuess: Double) = {
    def iterate (guess: Double): Double = {
      val next = f(guess)
      if (isCloseEnough(guess, next)) next
      else iterate(next)
    }
    iterate(firstGuess)
  }
  fixedPoint(x => 1 + x/2)(1)

  def sqrt(x: Double) = fixedPoint(y => x / y)(1)
  // this definition loops forever between 1.0 and 2.0 if you call:
  // sqrt(2) <--- infinite loop

  //("stabilizing by averaging")
  def sqrt2(x: Double) = fixedPoint(y => (y + x / y) / 2)(1)
  //this definition guesses using the average, so it converges:
  sqrt2(2)

  //TODO fazer o chines desse loop infinito e entender melhor pq a média converge

  //lets externalize the average fixed point:
  def averageDamp(f: Double => Double)(x: Double) = (x + f(x))/2
  def sqrt3(x: Double) =
    fixedPoint(averageDamp(y => x / y))(1)
  sqrt3(2)

}